from django.shortcuts import render
from django.views.generic import ListView , UpdateView
from blog.models import Product



class ProductListView(ListView):
      model = Product

class OutOfStockListView(ListView):
      model = Product

      def get_queryset(self):
          products = super().get_queryset()
          return products.filter(quantidade=0)

class AddStockView(UpdateView):
      model = Product

      def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantidade = self.request.POST['quantidade']
        product.quantidade += int(quantidade)
        product.save()
        return redirect('product-list')




class RemoveStockView(UpdateView):
      model = Product


      def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantidade = self.request.POST['quantidade']
        product.quantidade -= int(quantidade)
        product.save()
        return redirect('product-list')


     
